#include "listeObjets.h"
#include <iostream>

using namespace std;


listeObjets::listeObjets() {
	this->head = nullptr;
	this->tail = nullptr;
};

listeObjets::~listeObjets() {
	Objet2D* tmp;
	while (this->head != NULL) {
		tmp = this->head;
		this->head = tmp->getNext();
		delete tmp;
	}
};

void listeObjets::addAtHead(Objet2D* objet2D)
{
	Objet2D* current = this->head;
	this->head = objet2D;
	this->head->setNext(current);
}

void listeObjets::deleteAtHead() {
	Objet2D* current = this->head;
	this->head = this->head->getNext();
	delete current;
}

void listeObjets::deleteAtTail() {
	Objet2D* tmp = this->head;
	while (tmp->getNext() != this->tail) {
		tmp = tmp->getNext();
	}
	this->tail = tmp;
	tmp = tmp->getNext();
	this->tail->setNext(nullptr);
	delete tmp;
}


void listeObjets::addAtTail(Objet2D* objet2D)
{
	Objet2D* current = this->tail;
	this->tail = objet2D;
	current->setNext(this->tail);
}

void listeObjets::addAtIndex(Objet2D* tmpObj, int index) {
	int i = 0;
	Objet2D* tmp = this->head;
	while (i < index - 1) {
		tmp = tmp->getNext();
		i++;
	}
	tmpObj->setNext(tmp->getNext());
	tmp->setNext(tmpObj);
}


void listeObjets::afficherInfos() {
	Objet2D* tmp = this->head;
	int i = 0;
	while (tmp != nullptr) {
		cout << "Objet numero " << i + 1 << " " << tmp->afficheInfo() << endl;
		tmp = tmp->getNext();
		i++;
	};

};
