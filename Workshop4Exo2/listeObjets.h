#pragma once
#include "Objet2D.h"

class listeObjets {

private:
	Objet2D* head;
	Objet2D* tail;

public:
	listeObjets();
	~listeObjets();
	void addAtHead(Objet2D*);
	void addAtTail(Objet2D*);
	void addAtIndex(Objet2D*, int index);
	void deleteAtHead();
	void deleteAtTail();
	void deleteAtIndex(int index);
	void afficherInfos();
};
