#pragma once

#include "Objet2D.h"
#include "Rectangle.h"
#include "Carre.h"
#include "listeObjets.h"
#include <iostream>


int main() {
    listeObjets* objets;
    Rectangle* rect;
    Carre* carre;

    char o; int l, g, i, n;
    cout << "Choisissez le nombre des objets" << endl;
    cin >> n;
    objets = new listeObjets(n);
    for (i = 0; i < n; i++) {
        cout << "Objet numero " << i + 1 << " (R)ectangle ou (C)arre " << endl;
        do {
            cin >> o;
        } while ((o != 'R') && (o != 'r') && (o != 'C') && (o != 'c'));
        if ((o == 'R') || (o == 'r')) {
            cout << "Longueur du rectangle " << endl; cin >> l;
            cout << "Largeur du rectangle " << endl; cin >> g;
            rect = new Rectangle(l, g);
            objets->add(rect);
        }
        else {
            cout << "cote du cube " << endl; cin >> l;
            carre = new Carre(l);
            objets->add(carre);
        }
    }

    objets->afficherInfos();
    delete objets;
}
