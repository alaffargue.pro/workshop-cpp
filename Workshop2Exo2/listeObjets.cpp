#include "listeObjets.h"
#include <iostream>

using namespace std;

listeObjets::listeObjets(int tmptaille) :taille(tmptaille) {
	index = 0;
	tab = new Objet2D * [taille];
};

listeObjets::~listeObjets() { delete* tab; };

void listeObjets::add(Objet2D* tmpObj) {
	this->add(tmpObj, this->index);
	this->index++;
};

void listeObjets::add(Objet2D* tmpObj, int tmpindex) {
	tab[tmpindex] = tmpObj;
};

void listeObjets::afficherInfos() {
	int i;
	for (i = 0; i < this->taille; i++) {
		cout << "objet numero " << i + 1 << " " << tab[i]->afficheInfo() << endl;
	};
};
