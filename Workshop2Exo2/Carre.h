#pragma once
#include "Rectangle.h"

class Carre : public Rectangle {

public:
    Carre();
    Carre(int tcote);
    int getPerimetre();
    int getAire();
    int getCote();
    string afficheInfo();
};
