#pragma once
#include "Objet2D.h"

class listeObjets {

private:
	Objet2D** tab;
	int taille;
	int index;

public:
	listeObjets(int tmptaille);
	~listeObjets();
	void add(Objet2D* tmpObj);
	void add(Objet2D* tmpObj, int tmpindex);
	void afficherInfos();
};
