#include "Carre.h"
#include <iostream>
#include<string> 

using namespace std;

Carre::Carre() :Rectangle() {};
Carre::Carre(int tcote) : Rectangle(tcote, tcote) {};

int Carre::getPerimetre() {
	return Rectangle::getPerimetre();
};

int Carre::getAire() {
	return Rectangle::getAire();
};

int Carre::getCote() {
	return this->getLongueur();
};

string Carre::afficheInfo() { 
	return "l'objet est un cube : Cote = " + to_string(dim1) + ", Perimetre = " + to_string(getPerimetre()) + ", Aire = " + to_string(getAire()); 
};