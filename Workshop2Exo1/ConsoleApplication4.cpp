// ConsoleApplication4.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#pragma once
#include "Objet2D.h"
#include "Rectangle.h"
#include "Carre.h"
#include <iostream>

int main() {

    Objet2D* objet;
    cout << "Vous allez saisir les informaions pour un objet" << endl;
    char o; int l, g;
    cout << " (R)ectangle ou (C)arre " << endl;
    do {
        cin >> o;
    } while ((o != 'R') && (o != 'r') && (o != 'C') && (o != 'c'));
    if ((o == 'R') || (o == 'r')) {
        cout << "Longueur du rectangle " << endl; cin >> l;
        cout << "Largeur du rectangle " << endl; cin >> g;
        objet = new Rectangle(l, g);
    }
    else {
        cout << "cote du cube " << endl; cin >> l;
        objet = new Carre(l);
    }
    cout << objet->afficheInfo() << endl;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
