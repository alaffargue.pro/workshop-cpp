#include <iostream>
#include <string>

using namespace std;

class B {
public:
    void recevoir(string message) {
        cout << "recepetion du signal :" << message << endl;
    }
};

typedef void (B::* pointeurDeFonction)(string); 

pointeurDeFonction signal = &B::recevoir;

class A {
public:
    void envoyer(B o, string message) {
        (o.*signal)(message);
    }
};


int main()
{
	int pause;
	A a;
	B b, c;
	a.envoyer(b, "message 1");
	a.envoyer(c, "message 2");
	cin >> pause;
}