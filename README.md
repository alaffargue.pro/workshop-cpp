# Correction Workshop C++

## Name
Correction worskhop C++

## Description
Voici la correction des différents workshop de mes interventions C++ au CESI.

## Installation
Aucun installation nécessaire. Chaque workshop est un projet unique dans la solution Visual Studio.

## How to use - GIT
`git clone https://gitlab.com/alaffargue.pro/workshop-cpp.git`
