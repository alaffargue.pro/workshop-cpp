#pragma once
#include "Etudiant.h"

class Promo {
private:
	Etudiant* listEtudiants;
	int nb_etudiants;
public:
	Promo();
	int getNbEtudiants();
	void InputNotesUtilisateur();
	void afficherEtudiants();
	void InputNotesAUFichier(string t_fileName);
	void OutputNotesDUFichier(string t_fileName);
};
