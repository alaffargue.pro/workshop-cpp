// Workshop4Exo1.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>

//int main()
//{
//    // Création d'un tableau de 3 entiers
//    int tableau[3];
//
//    // Remplissage du tableau
//    std::cout << "Veuillez entrer trois entiers : " << std::endl;
//    for (int i = 0; i < 3; ++i) {
//        std::cout << "Entier " << i + 1 << " : ";
//        std::cin >> tableau[i];
//    }
//
//    // Affichage du contenu du tableau
//    std::cout << "Contenu du tableau : ";
//    for (int i = 0; i < 3; ++i) {
//        std::cout << tableau[i] << " ";
//    }
//    std::cout << std::endl;
//
//    return 0;
//}

//int main() {
//    //Création d'un tableau dynamique de 3 entiers
//    int taille;
//    std::cout << "Taille du tableau : " << std::endl;
//    std::cin >> taille;
//
//    int* tableau = new int[taille];
//     Remplissage du tableau
//    std::cout << "Veuillez entrer trois entiers : " << std::endl;
//    for (int i = 0; i < taille; ++i) {
//        std::cout << "Entier " << i + 1 << " : ";
//        std::cin >> tableau[i];
//    }
//
//     Affichage du contenu du tableau
//    std::cout << "Contenu du tableau : ";
//    for (int i = 0; i < taille; ++i) {
//        std::cout << tableau[i] << " ";
//    }
//    std::cout << std::endl;
//
//     Libération de la mémoire allouée pour le tableau dynamique
//    delete[] tableau;
//
//    return 0;
//}

//typedef struct nombre {
//	int n;
//	nombre* suivant;
//}N;
//
//void afficher(N* liste);
//
//void main(void) {
//	N* premiereAdresse;
//	N o1, o2, o3;
//	o1.n = 10; 
//	o2.n = 20; 
//	o3.n = 30;
//	premiereAdresse = &o1;
//	o1.suivant = &o2;
//	o2.suivant = &o3;
//	o3.suivant = NULL;
//	afficher(premiereAdresse);
//}
//
//void afficher(N* liste) {
//	while (liste != NULL) {
//		std::cout << liste->n << " ";
//		liste = liste->suivant;
//	}
//}


struct Node {
    int value;
    Node* next;
};

int main() {
    // Création des nœuds pour la liste chaînée
    Node* premier = new Node{ 1, nullptr };
    Node* deuxieme = new Node{ 2, nullptr };
    Node* troisieme = new Node{ 3, nullptr };

    // Liaison des nœuds pour former une liste chaînée
    premier->next = deuxieme;
    deuxieme->next = troisieme;

    std::cout << "Premier : " << premier << " " << &premier << " " << premier->next << " " << &premier->next << std::endl;
    std::cout << "Deuxieme : " << deuxieme << " " << &deuxieme << " " << deuxieme->next << " " << &deuxieme->next << std::endl;
    std::cout << "Troisieme : " << troisieme << " " << &troisieme << " " << troisieme->next << " " << &troisieme->next << std::endl;

    // Accès et affichage des valeurs à travers la liste chaînée
    Node* current = premier;
    while (current != nullptr) {
        std::cout << current->value << ": " << current << " " << &current << std::endl;
        current = current->next;
    }
    std::cout << std::endl;

    // Libération de la mémoire allouée pour les nœuds
    delete premier;
    delete deuxieme;
    delete troisieme;

    return 0;
}